import Vue from "vue";
import Vuex from "vuex";

import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {},
  state: {
    headerCategories: [],
  },
  mutations: {
    SET_HEADER_CATEGORIES(state, payload) {
      state.headerCategories = payload;
    },
  },
  actions: {
    async fetchHeaderCategories(context) {
      const reqHeaders = {
        "Content-Type": "application/json",
        // Authorization: "Bearer " + "qpmger4c12qx2t7ftfuldbl51w3js23c",
      };

      const query = `{ categoryList(filters: {ids: {eq: "3"}}) {id level include_in_menu position url_path name children_count children {id level include_in_menu position url_path name children_count children{ id level include_in_menu position url_path name children_count children{ id level include_in_menu position url_path name children_count}}}}}`;

      const resData = await axios({
        url:
          "https://m2.antarctica.test.e-bricks.cloud/graphql" +
          "?query=" +
          encodeURI(query),
        method: "GET",
        headers: reqHeaders,
      }).catch((e) => {
        console.error(e.message);
      });

      const payload = resData.data.data.categoryList[0].children;
      context.commit("SET_HEADER_CATEGORIES", payload);
    },
  },
  getters: {
    allCategories(state) {
      return state.headerCategories;
    },
  },
});
